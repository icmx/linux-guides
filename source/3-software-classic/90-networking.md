# Networking

## Common

### *ping* — Test

```
ping [options...] domain
```

Options:

  - `-c` count
  - `-s` size, bytes
  - `-i` interval, s
  - `-w` duration, s

This  command is extremely useful for domain or network availability checking.

### *ip* (replacement to old *ifconfig*, *route* and *arp*)

ifconfig = ip
route = ip r
arp = ip n

*see also*: *iw* (replacement to *iwconfig*)

## Download and Upload

### `wget`

```
  wget [options...] urls...
```

Options:

  - `-r` recursive downloading
  - `-H` span across hosts too
  - `-p` download all to properly display a page
  - `-k` convert the document links for local viewing
  - `-K` when converting, back up the original version
  - `-E` adjust page extension for `.aspx`, `.php` and same
  - `-U` set User Agent string
  - `-O` write all to file specified
  - `-w` retrievals interval, s
  - `-q` quiet (no logging)
  - `-e` execute command like in `wgetrc` file

`wget` is more (but not strictly) HTTP GET-oriented, (like download-oriented), better-appendable and works with pre-defined files (in `cp`-like manner).

See also `wgetrc` file syntax.

### `curl`

```
  curl [options...] urls...
```

Options:

  - `-d` send URL-encoded ASCII data (`key=value&...`)
  - `-T` send file
  - `-A` send User Agent string
  - `-b` send cookie (`key=value; ...`)
  - `-H` send header (`Key: Value`)
  - `-u` send `username` or `username:password`
  - `-e` send referrer URL
  - `-G` send GET requests instead of POST
  - `-c` set cookie storage file
  - `-K` set config file
  - `-o` set output file
  - `-s` silent (no logging)

`curl` is more (but not strictly) HTTP POST-oriented, (upload-oriented), single-shot and works with shell text streams (in `cat`-like manner).

See also `curlrc` file syntax.

## Secure Remote Access

### Secure Shell — `ssh`

```
  ssh [options...] user@host
```

Opens new shell session on target *host* machine under specified *user* (password may be prompted).

Target *host* must have running ssh daemon (e.g. `sshd`) with allowed user login to make this command work.

### Secure Copy — `scp`

```
  scp [options...] source target
```

Safe copy operation from or to remote machine, similar to generic [cp](#) tool (e.g. option `-r` is for recursive oprtation).

  - Source structure: `source-user@source-host:directory/file`
  - Target structure: `target-user@target-host:directory/file`

## Other

### *ss* — Socket Stats

### *nmap* — Explore Network
